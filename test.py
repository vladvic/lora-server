import json
import time
import base64
import binascii
import socket
import asyncio
import config
from LoRa.WAN import LoRaPacket
from LoRa.Device import deviceManager
from LoRa.Payload import concat_bytes, aes128_decrypt, aes128_encrypt, aes128_cmac


dev = deviceManager.find_device('3230333765387902', '7665676153564531')

AppSKey='4de674bc85f02129b357f089d1afaa98'
NwkSKey='0425f9bc7bd3641c79a66632b2cea98f'
JoinNonce=208673
DevNonce=41203
NetAddr=19
JoinEUI=binascii.unhexlify('7665676153564531')

pl = bytearray()
pl.append(0x02)
pl.extend(JoinNonce.to_bytes(3, 'little'))
pl.extend(NetAddr.to_bytes(3, 'little'))
pl.extend(DevNonce.to_bytes(2, 'big'))
pl.extend(b'\x00' * 7)
appSKey = binascii.hexlify(aes128_encrypt(dev.appKey, pl)).decode('ascii')

pl = bytearray()
pl.append(0x01)
pl.extend(JoinNonce.to_bytes(3, 'little'))
pl.extend(NetAddr.to_bytes(3, 'little'))
pl.extend(DevNonce.to_bytes(2, 'big'))
pl.extend(b'\x00' * 7)
nwkSKey = binascii.hexlify(aes128_encrypt(dev.appKey, pl)).decode('ascii')

print("AppSKey: {}; NwkSKey: {}".format(appSKey, nwkSKey))
#appSKey = '824f53e15699ec421985ce69e15f6264'

#data = binascii.unhexlify('20242f00130000432e01260005e8d983b8e18388e98358f18328f9830033369ffb')
data = base64.b64decode('QEMuASaAAAAEdBbna4rYuxaK')
data = base64.b64decode('QEMuASaAAQACB+6DO5vDflS/3omNQ4FvUc2CnAHVm4Lp')
packet = LoRaPacket(payload=data)
packet.message.decrypt_payload(appSKey)

print("Message: {}".format(packet.message.__dict__))

#print("Verification: {}".format(packet.verify(dev.appKey)))
#print("MIC: {}".format(binascii.hexlify(packet.mic)))


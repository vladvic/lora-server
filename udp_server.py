import time
import socket
import asyncio
import config
from LoRa.Proto import Packet, Handler
from LoRa.Device import deviceManager

def UdpProtocol(handler):
    class UdpProtocolClass(asyncio.DatagramProtocol):
        def __init__(self):
            self.handler = handler
            super().__init__()

        def connection_made(self, transport):
            self.transport = transport
            self.handler.socket = transport

        def datagram_received(self, data, addr):
            self.handler.handle(data, addr)
    return UdpProtocolClass

handler = Handler(0x000013)

@handler.handler('joined')
def process_joined(dev):
    print("Device joined: {}".format(dev.devEUI))

@handler.handler('stats')
def process_stats(stats):
    print("Stats: {}".format(stats))

loop = asyncio.get_event_loop()
t = loop.create_datagram_endpoint(UdpProtocol(handler), local_addr=('0.0.0.0', 8001))

loop.run_until_complete(t)
loop.run_forever()

import random
import binascii
from LoRa.Payload import concat_bytes, aes128_decrypt, aes128_encrypt, aes128_cmac
from Crypto.Hash import CMAC
from Crypto.Cipher import AES

class Device:
    def __init__(self, addr, **kwargs):
        self.devEUI  = '0000000000000000'
        self.appEUI  = '0000000000000000'
        self.appKey  = '00000000000000000000000000000000'
        self.netAddr = 0
        self.devAddr = addr
        self.nOnce  = 0
        self.devNOnce  = 0
        self.joinAcceptDelay1 = 5
        self.cfList = []
        self.__dict__.update(kwargs)
        self.calc_js_keys()

    def calc_js_keys(self):
        self.jsIntKey = binascii.hexlify(aes128_encrypt(self.appKey, concat_bytes(b'\x06', binascii.unhexlify(self.devEUI)[7::-1], b'\x00' * 7))).decode('ascii')
        self.jsEncKey = binascii.hexlify(aes128_encrypt(self.appKey, concat_bytes(b'\x05', binascii.unhexlify(self.devEUI)[7::-1], b'\x00' * 7))).decode('ascii')

    def calc_session_keys(self):
        pl = bytearray()
        pl.append(0x02)
        pl.extend(self.nOnce.to_bytes(3, 'little'))
        pl.extend(self.netAddr.to_bytes(3, 'little'))
        pl.extend(self.devNOnce.to_bytes(2, 'little'))
        pl.extend(b'\x00' * 7)
        self.appSKey = binascii.hexlify(aes128_encrypt(self.appKey, pl)).decode('ascii')

        pl[0] = 0x01
        self.fNwkSIntKey = binascii.hexlify(aes128_encrypt(self.appKey, pl)).decode('ascii')
        self.sNwkSIntKey = self.nwkSEncKey = self.fNwkSIntKey
        print("AppSKey: {}".format(self.appSKey))
        print("NwkSKey: {}".format(self.fNwkSIntKey))
        print("JoinNonce: {}".format(self.nOnce))
        print("DevNonce: {}".format(self.devNOnce))
        print("Net addr: {}".format(self.netAddr))
        print("pl: {}".format(pl))

class DeviceManager:
    def __init__(self):
        self.devices = dict()
        self.devAddr = 0x26012e43

    def add_device(self, **kwargs):
        device = Device(self.devAddr, **kwargs)
        self.devices[device.devEUI + ';' + device.appEUI] = device
        self.devAddr = (self.devAddr + 1) % 0xffffff

    def find_device(self, devEUI, appEUI):
        return self.devices.get(devEUI + ';' + appEUI, None)

    def find_by_addr(self, devAddr):
        for device in self.devices.values():
            if device.devAddr == devAddr:
                return device
        return None

deviceManager = DeviceManager()

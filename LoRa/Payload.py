import random
import sys
from binascii import unhexlify
from Crypto.Hash import CMAC
from Crypto.Cipher import AES

UP_LINK = 0
DOWN_LINK = 1

def concat_bytes(*args):
    payload = bytearray()
    for arg in args:
        payload.extend(arg)
    return bytes(payload)

def aes128_cmac(key, payload):
    key = bytes(bytearray.fromhex(key))
    cipher = CMAC.new(key, ciphermod=AES)
    cipher.update(payload)
    return cipher

def aes128_encrypt(key, payload):
    key = bytes(bytearray.fromhex(key))
    cipher = AES.new(key, AES.MODE_ECB)
    return cipher.encrypt(payload)

def aes128_decrypt(key, payload):
    key = bytes(bytearray.fromhex(key))
    cipher = AES.new(key, AES.MODE_ECB)
    return cipher.decrypt(payload)

def loramac_decrypt(payload, fcnt, key, dev_addr, direction=UP_LINK):
    """
    LoraMac decrypt
    Which is actually encrypting a predefined 16-byte block (ref LoraWAN
    specification 4.3.3.1) and XORing that with each block of data.
    payload_hex: hex-encoded payload (FRMPayload)
    sequence_counter: integer, sequence counter (FCntUp)
    key: 16-byte hex-encoded AES key. (i.e. AABBCCDDEEFFAABBCCDDEEFFAABBCCDD)
    dev_addr: 4-byte hex-encoded DevAddr (i.e. AABBCCDD)
    direction: 0 for uplink packets, 1 for downlink packets
    returns an array of byte values.
    This method is based on `void LoRaMacPayloadEncrypt()` in
    https://github.com/Lora-net/LoRaMac-node/blob/master/src/mac/LoRaMacCrypto.c#L108
    """
    #key = unhexlify(key)
    dev_addr = dev_addr.to_bytes(4, 'little')
    size = len(payload)

    bufferIndex = 0
    # block counter
    ctr = 1

    # output buffer, initialize to input buffer size.
    encBuffer = bytearray(b'\x00' * size)

    # For the exact definition of this block refer to
    # 'chapter 4.3.3.1 Encryption in LoRaWAN' in the LoRaWAN specification
    aBlock = bytearray(
        [
            0x01,  # 0 always 0x01
            0x00,  # 1 always 0x00
            0x00,  # 2 always 0x00
            0x00,  # 3 always 0x00
            0x00,  # 4 always 0x00
            direction,  # 5 dir, 0 for uplink, 1 for downlink
            dev_addr[0],  # devAddr lsb
            dev_addr[1],  # 1
            dev_addr[2],  # 2
            dev_addr[3],  # devAddr msb
            (fcnt & 0xFF),  # 10 sequence counter (FCntUp) lsb
            (fcnt >> 8) & 0xFF,  # 11 sequence counter
            (fcnt >> 16) & 0xFF,  # 12 sequence counter
            (fcnt >> 24) & 0xFF,  # 13 sequence counter (FCntUp) msb
            0x00,  # 14 always 0x01
            0x00,  # 15 block counter
        ]
    )

    # complete blocks
    while size >= 16:
        aBlock[15] = ctr & 0xFF
        ctr += 1
        #sBlock = aes_encrypt_block(aBlock)
        sBlock = aes128_encrypt(key, aBlock)
        for i in range(16):
            encBuffer[bufferIndex + i] = payload[bufferIndex + i] ^ sBlock[i]

        size -= 16
        bufferIndex += 16

    # partial blocks
    if size > 0:
        aBlock[15] = ctr & 0xFF
        sBlock = aes128_encrypt(key, aBlock)
        for i in range(size):
            encBuffer[bufferIndex + i] = payload[bufferIndex + i] ^ sBlock[i]

    return encBuffer

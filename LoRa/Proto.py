#from LoRa.Payload import loramac_decrypt
import asyncio
import struct
import random
import base64
import json
from aio_timers import Timer
from LoRa.Device import deviceManager
from LoRa.WAN import LoRaPacket, MessageType

class PacketType:
    PUSH_REQUEST = 0
    PUSH_ACK     = 1
    PULL_REQUEST = 2
    PULL_DATA    = 3
    PULL_ACK      = 4
    PULL_DATA_ACK = 5

class Packet:
    def __init__(self, packet=None):
        self.version = 2
        self.token = 0
        self.type = 1
        self.mac = None
        self.payload = bytearray()

        if(type(packet) is bytes):
            self.from_bytearray(packet)
        elif(type(packet) is Packet):
            self.version = packet.version
            self.token = packet.token
            self.type = packet.type

    def get_mac(self):
        if(self.mac is None):
            self.mac = "%0.2x:%0.2x:%0.2x:%0.2x:%0.2x:%0.2x:%0.2x:%0.2x" % struct.unpack("BBBBBBBB", self.payload[0:8])
            self.payload = self.payload[8:]
        return self.mac
    
    def from_bytearray(self, packet):
        self.version = int.from_bytes(packet[0:1], 'big')
        self.token = int.from_bytes(packet[1:3], 'big')
        self.type = int.from_bytes(packet[3:4], 'big')
        self.payload = packet[4:]

    def to_bytearray(self):
        packet = bytearray()
        packet.extend(self.version.to_bytes(1, 'big'))
        packet.extend(self.token.to_bytes(2, 'big'))
        packet.extend(self.type.to_bytes(1, 'big'))
        packet.extend(self.payload)
        return packet

class LoRaWanPacket:
    def __init__(self, data, addr):
        self.__dict__.update(**data)
        self.json = data
        if data.get('data') is not None:
            self.data = LoRaPacket(payload=base64.b64decode(self.data))
        self.addr = addr

    def to_json(self, appKey, **kwargs):
        json = self.json
        payload = self.data.to_bytes(appKey, **kwargs)

        json['size'] = len(payload)
        json['data'] = base64.b64encode(payload).decode("ascii")
        return json

class Handler(asyncio.DatagramProtocol):
    def __init__(self, nwk_id, socket = None):
        self.socket = socket
        self.network_id = nwk_id & 0xffffff
        self.gateways = dict()
        self.handlers = dict()
        self.join_acks = dict()
        super().__init__()

    def handler(self, htype):
        def register_handler(fn):
            self.handlers[htype] = fn
        return register_handler

    def process_stats(self, stats):
        if self.handlers.get('stats') is not None:
            self.handlers['stats'](stats)

    def call_handler(self, htype, *args):
        if self.handlers.get(htype) is not None:
            self.handlers[htype](*args)

    def process_join(self, device):
        if self.handlers.get('join') is not None:
            self.handlers['join'](device)

    def otaa_reply(self, dev, packet, trial = 0):
        dev.nOnce = (dev.nOnce + 1) % 0xffff
        json_obj = {
                        "tmst": packet.tmst + 4999000,
                        "ipol": True,
                        "freq": packet.freq,
                        "rfch": 0,
                        "modu": packet.modu,
                        "datr": packet.datr,
                        "codr": packet.codr,
                    }
        joinAck = LoRaPacket(mtype=1, join_nonce=dev.nOnce, network_id=self.network_id, dev_addr=dev.devAddr,
                              dl_settings=0, cf_list=dev.cfList)
        response = LoRaWanPacket(json_obj, packet.addr)
        response.data = joinAck
        json_obj = response.to_json(dev.appKey, decryptKey=dev.appKey)
        json_string = json.dumps({'txpk': json_obj}, separators=(',', ':'))
        pkt_id = self.push_data(json_string, response.addr)
        self.join_acks[pkt_id] = dev

    def process_payload(self, packet):
        if packet.data.mtype == 0:
            # OTAA join request
            request = packet.data.message
            device = deviceManager.find_device(request.dev_eui, request.app_eui)
            if device is not None:
                if packet.data.verify(device.appKey):
                    device.devNOnce = request.dev_nonce
                    device.netAddr = self.network_id
                    device.gateway = packet.addr
                    device.calc_session_keys()
                    #Timer(3.49, lambda : 
                    self.otaa_reply(device, packet)
                else:
                    print("Device {}; join packet verification failed".format(request.dev_eui))
            else:
                print("Device {}; {} not found".format(request.dev_eui, request.app_eui))
        elif packet.data.mtype == 1:
            # Ignore JoinAccept packets from other gateways
            pass
        elif packet.data.mtype == 2:
            device = deviceManager.find_by_addr(packet.data.message.dev_addr)
            if packet.data.message.fport == 0:
                packet.data.message.decrypt_payload(device.nwkSEncKey)
            else:
                packet.data.message.decrypt_payload(device.appSKey)
            print("Packet data: {} [{}]".format(packet.data.__dict__, packet.data.message.__dict__))
        else:
            print("Got packet type {}".format(packet.data.mtype))

    def push_data(self, message, addr):
        packet = Packet()
        packet.token = int(random.randint(0, 0xffff))
        packet.type = PacketType.PULL_DATA
        packet.payload = message.encode('ascii')
        self.send(packet.to_bytearray(), addr)
        return packet.token

    def parse_payload(self, payload, addr):
        json_data = json.loads(payload)
        if json_data.get('rxpk', None) is not None:
            for data in json_data['rxpk']:
                packet = LoRaWanPacket(data, addr)
                response = self.process_payload(packet)
        if json_data.get('stat', None) is not None:
            self.call_handler('stats', json_data['stat'])

    def handle(self, data, addr):
        packet = Packet(data)
        mac = packet.get_mac()

        print("{} =-> {}".format(addr, data))
        if packet.type == PacketType.PUSH_REQUEST: # PUSH data packet
            ack = Packet(packet)
            ack.type = PacketType.PUSH_ACK
            self.send(ack.to_bytearray(), addr)
            self.parse_payload(packet.payload.decode("utf-8"), self.gateways.get(mac, addr))
        elif packet.type == PacketType.PULL_REQUEST: # PULL data packet
            self.gateways[mac] = addr
            ack = Packet(packet)
            ack.type = PacketType.PULL_ACK
            self.send(ack.to_bytearray(), addr)
        elif packet.type == PacketType.PULL_DATA_ACK:
            if self.join_acks.get(packet.token, None) is not None:
                if len(packet.payload) == 0:
                    self.call_handler('joined', self.join_acks[packet.token])
                del self.join_acks[packet.token]
            if len(packet.payload) > 0:
                self.call_handler('tx_error', packet.payload.decode("utf-8"))

    def send(self, data, addr):
        print("{} <-= {}".format(addr, bytes(data)))
        self.socket.sendto(data, addr)


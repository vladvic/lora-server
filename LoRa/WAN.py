import binascii
import struct
from Crypto.Hash import CMAC
from Crypto.Cipher import AES
from LoRa.Payload import concat_bytes, aes128_decrypt, aes128_encrypt, aes128_cmac, loramac_decrypt

class MessageType:
    JOIN_REQUEST = 0
    JOIN_ACCEPT  = 1
    UNCONFIRMED_DATA_UP   = 2
    UNCONFIRMED_DATA_DOWN = 3
    CONFIRMED_DATA_UP   = 4
    CONFIRMED_DATA_DOWN = 5
    REJOIN_REQUEST = 6

class UnconfirmedDataUp:
    def __init__(self, **kwargs):
        payload = kwargs.get('payload', None)
        self.payload = payload
        if payload is not None:
            self.dev_addr = int.from_bytes(payload[0:4], 'little') 
            self.fctrl = int.from_bytes(payload[4:5], 'little') 
            self.fcnt = int.from_bytes(payload[5:7], 'little') 
            self.fopts_encrypted = bytearray()
            self.fopts_decrypted = None
            optlen = self.fctrl & 0x0f
            if optlen > 0:
                self.fopts_encrypted.extend(payload[7:7+optlen])
            if len(payload) > (6 + optlen):
                self.fport = int.from_bytes(payload[7+optlen:8+optlen], 'little') 
            self.payload_encrypted = payload[8+optlen:]
            self.payload_decrypted = None

    def decrypt_fopts(self, key):
        if self.fopts_decrypted is None:
            self.fopts_decrypted = aes128_decrypt(key, self.fopts_encrypted) #loramac_decrypt(self.fopts_encrypted, fcnt, key, dev_addr)
        return self.fopts_decrypted

    def decrypt_payload(self, key):
        if self.payload_decrypted is None:
            self.payload_decrypted = loramac_decrypt(self.payload_encrypted, self.fcnt, key, self.dev_addr)
        return self.payload_decrypted

    def to_bytes(self, **kwargs):
        if self.payload is None:
            self.payload = bytearray()

        return self.payload

    def mtype(self = None):
        return MessageType.UNCONFIRMED_DATA_UP

class JoinOTAAResponse:
    def __init__(self, **kwargs):
        payload = kwargs.get('payload', None)
        self.payload = payload
        self.join_nonce = kwargs.get('join_nonce', 0)
        self.network_id = kwargs.get('network_id', 0)
        self.dev_addr = kwargs.get('dev_addr', 0)
        self.dl_settings = kwargs.get('dl_settings', 0)
        self.rx_delay = kwargs.get('rx_delay', 5)
        self.cf_list = kwargs.get('cf_list', [])
        if payload is not None:
            self.join_nonce = int.from_bytes(payload[0:3], 'little') 
            self.network_id = int.from_bytes(payload[3:6], 'little') 
            self.dev_addr = int.from_bytes(payload[6:10], 'little') 
            self.dl_settings = int.from_bytes(payload[10:11], 'little') 
            self.rx_delay = int.from_bytes(payload[11:12], 'little') 
            self.cf_list = []
            for idx in range(12, len(payload), 3):
                self.cf_list.append(100 * int.from_bytes(payload[idx:idx+3], 'little'))

    def to_bytes(self, **kwargs):
        if self.payload is None:
            self.payload = bytearray()
            self.payload.extend(self.join_nonce.to_bytes(3, 'little'))
            self.payload.extend(self.network_id.to_bytes(3, 'little'))
            self.payload.extend(self.dev_addr.to_bytes(4, 'little'))
            self.payload.extend(self.dl_settings.to_bytes(1, 'little'))
            self.payload.extend(self.rx_delay.to_bytes(1, 'little'))
            for freq in self.cf_list:
                self.payload.extend(int(freq / 100).to_bytes(3, 'little'))
            padding_size = (16 - (len(self.cf_list) * 3 % 16)) % 16
            padding = b'\x00' * padding_size
            self.payload.extend(padding)

        return self.payload

    def mtype(self = None):
        return MessageType.JOIN_ACCEPT

class JoinOTAARequest:
    def __init__(self, **kwargs):
        payload = kwargs.get('payload', None)
        self.payload = payload
        self.app_eui = kwargs.get('app_eui', '0000000000000000')
        self.dev_eui = kwargs.get('dev_eui', '0000000000000000')
        self.dev_nonce = kwargs.get('dev_nonce', 0)
        if payload is not None:
            self.app_eui = "%0.2x%0.2x%0.2x%0.2x%0.2x%0.2x%0.2x%0.2x" % struct.unpack("BBBBBBBB", payload[7::-1])
            self.dev_eui = "%0.2x%0.2x%0.2x%0.2x%0.2x%0.2x%0.2x%0.2x" % struct.unpack("BBBBBBBB", payload[15:7:-1])
            self.dev_nonce = int.from_bytes(payload[16:18], 'little')

    def to_bytes(self, **kwargs):
        if self.payload is None:
            self.payload = bytearray()
            self.payload.extend(struct.pack("cccccccc", *self.app_eui)[7::-1])
            self.payload.extend(struct.pack("cccccccc", *self.dev_eui)[7::-1])
            self.payload.extend(self.dev_nonce.to_bytes(2, 'little'))

        return self.payload

    def mtype(self = None):
        return MessageType.JOIN_REQUEST

class MessageFactory:
    def __init__(self):
        self.messages = dict()

    def register(self, mclass):
        self.messages[mclass.mtype()] = mclass

    def create(self, message_type, **kwargs):
        mclass = self.messages.get(message_type, None)

        if mclass is not None:
            return mclass(**kwargs)

        return None

messageFactory = MessageFactory()
messageFactory.register(JoinOTAARequest)
messageFactory.register(JoinOTAAResponse)
messageFactory.register(UnconfirmedDataUp)

class LoRaPacket:
    def __init__(self, **kwargs):
        if kwargs.get('payload', None):
            data = bytearray(kwargs['payload'])
            self.mhdr = int.from_bytes(data[0:1], 'little')
            self.mtype = self.mhdr >> 5
            if kwargs.get('encryptKey'):
                data[1:] = self.encrypt(kwargs['encryptKey'], data[1:])
            if kwargs.get('decryptKey'):
                data[1:] = self.decrypt(kwargs['decryptKey'], data[1:])
            self.mic = data[-4:]
            kwargs['payload'] = data[1:-4]

        if kwargs.get('mtype', None) is not None:
            self.mtype = kwargs['mtype']
            self.mhdr = self.mtype << 5

        self.message = messageFactory.create(self.mtype, **kwargs)

    def decrypt(self, key, payload):
        padding_size = (16 - (len(payload) % 16)) % 16
        padding = b'\x00' * padding_size
        if padding_size > 0:
            print("Payload size is not a multiply of 16!")
        return aes128_decrypt(key, concat_bytes(payload, padding))

    def encrypt(self, key, payload):
        padding_size = (16 - (len(payload) % 16)) % 16
        padding = b'\x00' * padding_size
        if padding_size > 0:
            print("Payload size is not a multiply of 16!")
        return aes128_encrypt(key, concat_bytes(payload, padding))

    def to_bytes(self, appKey, **kwargs):
        self.mic = self.calc_mic(appKey)
        payload = concat_bytes(self.message.to_bytes(**kwargs), self.mic)
        if kwargs.get('encryptKey'):
            payload = self.encrypt(kwargs['encryptKey'], payload)
        if kwargs.get('decryptKey'):
            payload = self.decrypt(kwargs['decryptKey'], payload)
        return concat_bytes(self.mhdr.to_bytes(1, 'little'), payload)

    def calc_mic(self, appKey):
        return aes128_cmac(appKey, concat_bytes(self.mhdr.to_bytes(1, 'little'), self.message.to_bytes())).digest()[:4]

    def verify(self, appKey):
        mic = self.calc_mic(appKey)
        return mic == self.mic


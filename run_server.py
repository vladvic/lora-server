import flask
from config import *
import os

server = flask.Flask(__name__)

@server.route('/<path:path>', methods=['POST'])
def get_message(path):
    print("POST request: {}".format(path))
    return "", 200


@server.route('/<path:path>', methods=["GET"])
def index(path):
    print("GET request: {}".format(path))
    return "", 200


if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('WS_PORT', 8001)))

